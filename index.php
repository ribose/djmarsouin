<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="css.css">
  <title>DJ Marsouin</title>
</head>
<body>
  <div class="container">
    <div class="row border-bottom">
      <div class="col-lg-3 col-md-3 col-sm-12">
        <img  class="logo1-2" src="./images/logo1-2.png" alt="">
      </div>
      <div class="col-lg-9 col-md-9 col-sm-12">
        <img  class="jr float-right" src="./images/jr.png" alt="">
      </div>
    </div>
    <!---PRESENTATION-->
    <div class="presentation" id="presentation">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="vertical-menu border-right border-bottom">
            <a href="#presentation" class="noir">Présentation</a>
            <a href="#podcasts" class="rouge">Podcasts</a>
            <a href="#prochainesdates" class="noir">Prochaines dates</a>
            <a href="#performances" class="rouge">Performances passées</a>
            <a href="#contact" class="noir">Contact</a>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12" >
          <h1>Présentation</h1>
          <img class="logo1-2"  src="./images/logorond.png"  alt="">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 presentationtxt">
          La musique est une passion! Pour ambiancer vos soirées au son
          des hits d’hier et d’aujourd’hui, DJ Marsouin est prêt à vous
          divertir jusqu’au petit matin. Depuis plusieurs années, j’anime
          aussi bien des fêtes villageoises, que des mariages et des
          anniversaires.
          Si vous souhaitez difuser sur Facebook Live, flmer et faire des
          montages de vos événements, n’hésitez pas à me contacter. Je
          mets volontiers mon expérience pratique à votre disposition. Ce
          média m’est familier, car je flme et réalise régulièrement des
          interviews pour le judo et le hockey sur glace.
        </div>
      </div>
    </div>
    <!---PODCASTS-->
    <div class="Podcasts" id="podcasts">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">

        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 border-top">
          <h1>Podcasts</h1>
          <img class="video" src="./images/video.png" alt="">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12">
          <button type="button" class="btn btn-outline-light btnpodcasts" name="button"><a href="podcasts.php" class="texte"> Voir plus de Podcasts</a></button>
        </div>
      </div>
    </div>

    <!---PROCHAINE DATES-->
    <div class="Prochaines_dates" id="prochainesdates">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">

        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 border-top">
          <h1>Prochaines dates</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 ">
          <table class="table texte text-center" style="margin-top:20px;">
            <thead>
              <tr>
                <th scope="col"></th>
                <th scope="col">Dates</th>
                <th scope="col">Emplacement</th>
                <th scope="col">Etablissement</th>
                <th scope="col">Horaire</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row"></th>
                <td>01-12-20</td>
                <td>Bienne</td>
                <td>La bombonne</td>
                <td>20h-00h</td>
              </tr>
              <tr>
                <th scope="row"></th>
                <td>08-04-22</td>
                <td>Neuchâtel</td>
                <td>Machin</td>
                <td>21h-03h</td>
              </tr>
              <tr>
                <th scope="row"></th>
                <td>01-12-23</td>
                <td>Boudry</td>
                <td>Truc</td>
                <td>20h-01h</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!---PERFORMMANCES PASSEES-->
    <div class="performances" id="performances">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">

        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 border-top">
          <h1>Performances Passées</h1>
        </div>
      </div>
      <div class="row perf">
        <div class="col-lg-3 col-md-3 col-sm-12">
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12">
          <img src="./images/performances.png" class="video" alt="">
        </div>
      </div>
    </div>

    <!---CONTACT-->
    <div class="contact" id="contact">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">

        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 border-top">
          <h1>Contact</h1>
        </div>
      </div>
      <div class="row texte">
        <div class="col-lg-3 col-md-3 col-sm-12">
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12">
          <form  method="POST" action="envoie.php" name="formulaire">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label>Nom* :</label>
                <input type="text" class="form-control" name="nom" required>
              </div>
              <div class="form-group col-md-6">
                <label>Prénom* :</label>
                <input type="text" class="form-control" name="prenom" required >
              </div>
              <div class="form-group col-md-6">
                <label>Email* :</label>
                <input type="email" class="form-control" name="email" required>
              </div>
              <div class="form-group col-md-6">
                <label>Nom de la société/Association :</label>
                <input type="text" class="form-control" name="societe" >
              </div>
            </div>
            <div class="form-group">
              <label>Description de la demande* :</label>
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description" required></textarea>
            </div>
            <button type="submit" class="btn btn-outline-light" name="send" style="margin-bottom:20px;">Envoyer</button>
          </form>
        </div>
      </div>
    </div>
    <?php require_once('footer.php'); ?>
  </div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
</html>
