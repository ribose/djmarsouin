<!DOCTYPE html>
<html lang=fr dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="css.css">
    <title>Podcasts - DJMarsouin</title>
  </head>
  <body>
    <div class="container">
      <div class="row border-bottom">
        <div class="col-lg-3 col-md-3 col-sm-12">
          <img  class="logo1-2" src="./images/logo1-2.png" alt="">
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12">
          <img  class="jr float-right" src="./images/jr.png" alt="">
        </div>
      </div>

      <div class="Podcasts" id="podcasts">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <h1>Podcasts</h1>
          </div>
          </div>
          <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
              <h6>Podcast du 10/20/2020</h6>
              <img class="ExemplePodcast" src="./images/video.png" alt="">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
            <center><button type="button" class="btn btn-outline-light btnpodcasts" name="button"><a href="./index.php" class="texte">Retour à la page d'accueil</a></button></center>
          </div>
          </div>
        </div>
        <?php require_once('footer.php'); ?>
    </div>
  </body>
</html>
